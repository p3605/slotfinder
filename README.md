## Name
slotfinder

## Description
Ein Raspberry Pi auf einer 8-fach Relaiskarte, etwas Software zur Datenspeicherung und Visualisierung und Python-Skripte zur Auswertung einer Konfigurationsdatei und Einlesen von
stündlichen Strompreisen über die aWATTar API.

![slotfinder negative stock price](/media/slotfinder_guteZeiten.png "negative stock price")

aWATTar bietet einen Stromtarif mit stündlichen Preisen an. Die Preise orientieren sich am Börsenpreis plus diversen Aufschlägen. Die Herausforderung besteht darin, die jeden Tag anders liegenden Preistäler für stromhungrige Geräte zu finden und über Relais dann Freigaben zu schaffen.

Eine Konfigurationsdatei erlaubt es beliebige Aufgaben (Tasks) zu definieren. Darin enthalten
sind Angaben, ob der Task gerade benutzt werden soll, wie lange er aktiv sein soll und in welchem Zeitraum ein Preistal gesucht werden soll. Jedem Task ist eine Kommandodatei zugeordnet für das Einschalten und das Ausschalten.

![slotfinder data flow](/media/slotfinder_Datenfluss.png "data flow")

Das Python-Skript schreibt eine ToDo-Liste für das Betriebssystem (Crontab) mit den Uhrzeiten
für den Aufruf der Kommandodateien. Diese Crontab wird täglich neu erstellt.
Der Aufruf der Python-Skripte zur Preisabfrage und zur Slot-Findung stehen auch in einer
Crontab und werden jeden Tag gestartet. Die Preisabfrage startet um 23.00 Uhr und die Slot-
Findung um 23.50 Uhr.

![slotfinder Ergebnis](/media/slotfinder_Preistalkaufen.png "decided well")
Die gepunktete Linie ist der stündliche Preis auf aWATTar. Darunter ist in rot die bezogene Energie aufgezeigt. Hier lässt sich erkennen, dass der Strombezug steigt wenn der Preis fällt. **Das ist das Ziel des slotfinders: möglichst viele Lasten in günstige Zeiträume verlegen.**

## Konfiguration
### .conf Datei
Eingabe: `nano scripte/awattar_scheduler.conf`

![slotfinder config file](/media/slotfinder_nano_awattar_scheduler_conf_2.png "config file")

Jeder Task beginnt mit dem Schlüsselwort `Task_` und muss in `[ ]` geschrieben sein. Es können
beliebig viele Tasks angelegt werden.
Im Beispiel ist der `Task_2BWWP` für die Nachtspeicherheizung im Bad an Relais 1 gedacht.
Mit `enable = true` wird der Task berücksichtigt. Wird hier `false` eingetragen, wird für diesen
Task kein Zeitslot gesucht.
`starttime = 0` und `periode = 12` sagt, dass zwischen 0 Uhr und 12 Uhr ein Zeitslot gesucht
wird.
`duration = 2` bestimmt den Zeitslot mit der Dauer von 2 Stunden. Dieser Zeitslot kann zu belie-
biger voller Stunde zwischen 0 und 12 Uhr beginnen. Achtung, er kann auch um 12 Uhr begin-
nen und dann bis 14.00 Uhr gehen.
`commando_start`und `commando_stop` werden so in die User-Crontab übernommen. Diese sind
voreingestellt für die 8 Relais.


### bash scripte für Relais ein/aus

![slotfinder bash script](/media/slotfinder_bash-script.png "bash script example")

diese bash-scripte werden in der `scripte/awattar_scheduler.conf` unter `commando_start` und `commando_stop` verlinkt. Im Script selbst wird `gpio` genutzt um die einzelnen Relais über die BCM channel-nummern anzusprechen. Es wird eine Ausgabe definiert, die dann in die Logdatei übergeben wird. 
Ein CURL-Kommando schreibt direkt in die Influx-Datenbank um den Zustand des Relais abbilden zu können.


## Visuals



# Inbetriebnahme

Es handelt sich um keine Fertiglösung sondern um einen Bausatz.

Der Raspberry Pi muss mit der Oberseite auf die Relaiskarte gesetzt werden. Die 40polige
Stiftleiste passt in die 40polige Buchenleiste.
Die Relaiskarte wird auf eine Hutschiene gesteckt. Davor werden die Seitenteile mit den beiliegenden Schrauben fixiert.

Die SD-Karte muss in den Slot gesteckt werden, so, dass die Kontakte zur Platinenseite zeigen.
Ein Ethernet-Kabel wird vom Switch zum Port am Raspberry geführt und eingesteckt.

**Die Relais werden von der Elektrofachkraft verdrahtet.**

Das Netzkabel wird eingesteckt und der RaspberryPi wird eingeschaltet.

